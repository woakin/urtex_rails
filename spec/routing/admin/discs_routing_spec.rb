require "rails_helper"

RSpec.describe Admin::DiscsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/admin/discs").to route_to("admin/discs#index")
    end

    it "routes to #new" do
      expect(:get => "/admin/discs/new").to route_to("admin/discs#new")
    end

    it "routes to #show" do
      expect(:get => "/admin/discs/1").to route_to("admin/discs#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/admin/discs/1/edit").to route_to("admin/discs#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/admin/discs").to route_to("admin/discs#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/admin/discs/1").to route_to("admin/discs#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/admin/discs/1").to route_to("admin/discs#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/admin/discs/1").to route_to("admin/discs#destroy", :id => "1")
    end

  end
end
