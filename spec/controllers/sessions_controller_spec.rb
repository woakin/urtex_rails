require 'rails_helper'
require 'support/macros'

RSpec.describe SessionsController, type: :controller do

  describe '#new' do
    context 'not loged in user' do
      before do
        get :new
      end
      it  'should recibe a succesfull http code' do
        expect(response).to have_http_status :success
      end
    end

    context 'loged in user' do

      it 'should redirect to home page' do
        user = Fabricate(:user)
        log_in user
        get :new
        expect(response).to redirect_to root_path
      end
    end
  end

  describe '#create' do
    let(:user){Fabricate(:user)}
    context 'succesfully login as user' do
      before do
        post :create, {email: user.email,password: user.password}
      end

      it 'should create the session' do
        expect(session[:user_id]).to eq(user.id)
      end

      it 'should set a success msg' do
        expect(flash[:success]).to be_truthy
      end

      it 'should redirect to home page' do
        expect(response).to redirect_to root_path
      end

    end

    context 'succesfully login as admin' do
      let(:admin){Fabricate(:user,admin: true)}
      before do
        post :create, {email: admin.email,password: admin.password}
      end

      it 'should create the session' do
        #require 'pry';binding.pry
        expect(session[:user_id]).to eq(admin.id)
      end

      it 'should set a success msg' do
        expect(flash[:success]).to be_truthy
      end

      it 'should redirect to admin path' do
        expect(response).to redirect_to admin_panel_path
      end

    end


    context 'unsuccesfull login' do
      before do
        post :create, {email: user.email,password: nil}
      end
      it 'should not create session user_id' do
        expect(session[:user_id]).to be_falsy
      end

      it 'should record the error msg' do
        expect(flash[:danger]).to be_truthy
      end

      it 'should redirect to login page' do
        expect(response).to redirect_to new_user_path
      end
    end
  end

  describe '#destroy' do
    let(:user){Fabricate(:user)}

    context 'loged in user' do
      before do
        log_in user
        delete :destroy, id: user.id
      end

      it 'should log out the user' do
        expect(session[:user_id]).to be_nil
      end

      it 'should set a confirmation msg' do
        expect(flash[:success]).to be_truthy
      end

      it 'should redirect to home page' do
        expect(response).to redirect_to root_path
      end
    end

    context 'not loged in user' do

      before do
        delete :destroy, id: rand(100)
      end

      it 'should redirect to root path' do
        expect redirect_to root_path
      end


    end

  end
end
