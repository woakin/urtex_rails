require 'rails_helper'
require 'support/macros'

RSpec.describe UsersController, type: :controller do
  describe '#new' do
    let(:user){Fabricate(:user)}

    context 'is loged user' do
      it 'should redirect to root path' do
        log_in user
        get :new
        expect(response).to redirect_to root_path
      end

    end

    context 'is admin' do
      let(:admin){Fabricate(:user,admin:true)}
      before do
        log_in admin
        get :new
      end
      it 'should redirect to admin index' do
        expect(response).to redirect_to root_path
      end

    end

    context 'is guest' do
      it 'should get a succesfull http request' do
        get :new
        expect(response).to have_http_status :success
      end
    end

  end

  describe '#create' do
    context 'succesfull create' do
      let(:new_user){Fabricate.attributes_for(:user)}

      before do
        #require 'pry';binding.pry
        post :create, user: new_user
      end

      it 'should save the user' do
        expect(User.count).to eq(1)
      end

      it 'should log in the user' do
        expect(session[:user_id]).to eq(User.last.id)
      end

      it 'should redirect to home page' do
        expect(response).to redirect_to root_path
      end

    end

    context 'unsucessfull create' do
      let(:user){Fabricate.attributes_for(:user,email: 'asd')}

      before do
        post :create, user: user
      end

      it 'should not save the record' do
        expect(User.count).to eq(0)
      end

      it 'should save the errors ' do
        #require 'pry';binding.pry
        expect(flash[:danger]).to be_truthy
      end

      it 'shoudl redirect to new user path' do
        expect(response).to redirect_to new_user_path
      end
    end

  end


end
