require 'rails_helper'

RSpec.describe "admin/featureds/show", type: :view do
  before(:each) do
    @admin_featured = assign(:admin_featured, Admin::Featured.create!(
      :url => "Url",
      :title => "Title"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Url/)
    expect(rendered).to match(/Title/)
  end
end
