require 'rails_helper'

RSpec.describe "admin/featureds/edit", type: :view do
  before(:each) do
    @admin_featured = assign(:admin_featured, Admin::Featured.create!(
      :url => "MyString",
      :title => "MyString"
    ))
  end

  it "renders the edit admin_featured form" do
    render

    assert_select "form[action=?][method=?]", admin_featured_path(@admin_featured), "post" do

      assert_select "input#admin_featured_url[name=?]", "admin_featured[url]"

      assert_select "input#admin_featured_title[name=?]", "admin_featured[title]"
    end
  end
end
