require 'rails_helper'

RSpec.describe "admin/interpreters/index", type: :view do
  before(:each) do
    assign(:admin_interpreters, [
      Admin::Interpreter.create!(
        :name => "Name"
      ),
      Admin::Interpreter.create!(
        :name => "Name"
      )
    ])
  end

  it "renders a list of admin/interpreters" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
