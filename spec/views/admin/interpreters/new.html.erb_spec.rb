require 'rails_helper'

RSpec.describe "admin/interpreters/new", type: :view do
  before(:each) do
    assign(:admin_interpreter, Admin::Interpreter.new(
      :name => "MyString"
    ))
  end

  it "renders new admin_interpreter form" do
    render

    assert_select "form[action=?][method=?]", admin_interpreters_path, "post" do

      assert_select "input#admin_interpreter_name[name=?]", "admin_interpreter[name]"
    end
  end
end
