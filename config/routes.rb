Rails.application.routes.draw do

  namespace :admin do
    get '/', to: 'statics#admin_index', as: 'panel'
    resources :discs
    resources :authors
    resources :genres
    post "tracks/:id", to: 'tracks#create', as: 'disc_for_tracks'
    put "tracks/:id", to: 'tracks#updateSampleUrl', as: 'edit_track'
    delete "tracks/:id", to: "tracks#destroy", as: 'track_destroy'
    post "attachments/:attachmentable_type/:attachmentable_id", to: 'attachments#create', as: 'attachment'
    delete "attachments/:id", to: "attachments#destroy", as: 'attachment_destroy'
    resources :tracks, only:[:edit,:update] do
      member do
        get :setPrice
      end
    end
    resources :attachments, only:[:create]
    resources :featureds
    resources :interpreters
  end

  get 'orders/download/:url', to: 'orders#download', as: 'download'
  root 'statics#homepage'
  resource :user
  resources :sessions, only: [:create,:destroy,:new]

  resources :discs, only: [:show] do
    collection do
      get :search, to: 'discs#search'
      get 'search/:disc', to: 'discs#search'
    end
  end

  get 'checkout', to: 'checkout#show', as: 'checkout'
  resource :payments, only: [:create]
  get 'payments/confirm', to: 'payments#confirm', as: 'payment_confirm'
  get 'payments/cancel', to: 'payments#cancel', as: 'payment_cancel'
  resources :orders, only: [:show]
  resource :profile, only: [:show]
  resources :catalogs, only: [:show,:index]
  get 'acerca_de', to: 'statics#acerca_de', as: 'acerca_de'
  get 'locals/set/:id',to: 'locals#set',as:'change_language'
end
