// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require turbolinks
//= require jquery.fileupload
//= require s3_direct_upload
//= require js.cookie
//= require bootstrap.min
//= require jquery-ui
//= require_tree .


$(document).ready(function(){

  var favoriteButtons = $(".favoriteButton");

  $.each(favoriteButtons,function(){
    var number = parseInt($(this).attr("number"));
    var type = $(this).attr("type");

    if(itemIsOnBasket(number,type)){
      $(this).addClass("remove-basket");
    }
    else{
      $(this).addClass("add-basket");
    }
  });

  $("body").on("click",'.add-basket',addBasket);
  $("body").on("click", '.remove-basket',removeBasket);

});

function removeBasket(){
  var basket = returnCookiesArrayOrEmptyArray();
  var id = parseInt($(this).attr("number"));
  var type = $(this).attr("type");

  var arrayWithoutCurrentItem  = $.each(basket, function(item){
    if(basket[item].type === type && basket[item].id == id) {
        basket.splice(item,1);
        return false;
    }
  });
  decreaseCookieCounter();
  showRemoveMsg();
  Cookies("basket", JSON.stringify(arrayWithoutCurrentItem),{expires: 365})
  $(this).removeClass("remove-basket");
  $(this).addClass("add-basket")
}

function addBasket(){
  var basket = returnCookiesArrayOrEmptyArray();

  var id = parseInt($(this).attr("number"));
  var type = $(this).attr("type");

  var item = {id: id, type: type};
  basket.push(item);
  Cookies("basket", JSON.stringify(basket),{ expires: 365 });
  increaseCookieCounter();
  showPurchaseMsg();
  $(this).removeClass("add-basket");
  $(this).addClass("remove-basket")
}

function itemIsOnBasket(id,type){
   cookies = returnCookiesArrayOrEmptyArray();
   var itemExists = cookies.filter(function(object){ return object.id == id && object.type == type });

   if(itemExists === undefined || itemExists.length == 0){
     return false;
   }
   else{
     return true;
   }
}

function returnCookiesArrayOrEmptyArray(){
  cookies = Cookies("basket") || [];
  if (cookieHaveContent(cookies)){
    return stringToObject(cookies);
  }
  else{
    return cookies;
  }
}

function increaseCookieCounter(){
  element = document.getElementById("cookie_counter");
  counter = parseInt(element.innerHTML);
  counter += 1;
  element.innerHTML =  counter;
}

function decreaseCookieCounter(){
  element = document.getElementById("cookie_counter");
  counter = parseInt(element.innerHTML);
  counter -= 1;
  element.innerHTML =  counter;
}

function showPurchaseMsg(){
  $("#purchaseMsg").show();
}

function showRemoveMsg(){
  $("#removeMsg").show();
}

function cookieHaveContent(cookie){
  if(typeof(cookies)== "string"){
    return true;
  }
  else{
    return false;
  }
}

function stringToObject(string){
  return JSON.parse(string);
}
