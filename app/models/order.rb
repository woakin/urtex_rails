class Order < ActiveRecord::Base
  has_many :details
  has_many :discs, :through => :details, :source => :detailmentable, :source_type => 'Disc'
  has_many :tracks, :through => :details, :source => :detailmentable, :source_type => 'Track'
  belongs_to :user
  has_one :payment
end
