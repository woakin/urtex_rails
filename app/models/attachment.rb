class Attachment < ActiveRecord::Base

  validates :url, presence: true
  validates :amazonId, presence: true, uniqueness: true

  belongs_to :attachmentable, polymorphic: true
end
