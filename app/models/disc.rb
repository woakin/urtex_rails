class Disc < ActiveRecord::Base
  translates :title,:description

  validates :title, presence: true
  validates :price, presence: true
  has_many :attachments, as: :attachmentable
  has_many :tracks
  belongs_to :genre
  has_many :performances
  has_many :interpreters, :through => :performances

  has_many :shelves
  has_many :authors, :through => :shelves

  def self.search_by_title title
    where("title_es ILIKE ? or title_en LIKE ?", "%#{title}%","%#{title}%")
  end

end
