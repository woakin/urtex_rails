json.array!(@admin_featureds) do |admin_featured|
  json.extract! admin_featured, :id, :url, :title
  json.url admin_featured_url(admin_featured, format: :json)
end
