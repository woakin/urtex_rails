json.array!(@admin_interpreters) do |admin_interpreter|
  json.extract! admin_interpreter, :id, :name
  json.url admin_interpreter_url(admin_interpreter, format: :json)
end
