json.array!(@admin_discs) do |admin_disc|
  json.extract! admin_disc, :id
  json.url admin_disc_url(admin_disc, format: :json)
end
