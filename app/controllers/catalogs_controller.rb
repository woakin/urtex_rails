class CatalogsController < ApplicationController

  before_action :set_genre, only:[:show]

  def index
    @minPrice = Disc.order('price asc').first.price
    @maxPrice = Disc.order('price desc').first.price
    @discs = Disc.limit(9)
  end

  def show

  end

  def search
    @discs = Disc.search_by_title(params[:title])
  end

  private
  def set_genre
    @genre = Genre.find(params[:id])
  end
end
