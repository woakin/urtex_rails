class PaymentsController < ApplicationController

  before_action :require_login_to_buy

  def require_login_to_buy
    unless current_user
      flash[:danger] =  t("flash_necesitas_crear_cuenta")
      redirect_to new_user_path
    end
  end

  def create
    @order = Order.new

    if current_user
      @order.user_id = current_user.id
    end

    @items = jsonToActiveRecord(JSON.load(cookies[:basket]))
    @order.totalPrice = totalPrice @items

    if @order.save
      @items.each do |item|
        Detail.create({
          detailmentable_id: item.id,
          detailmentable_type: item.class.name,
          order_id: @order.id
        })
      end
    end


    request = Paypal::Express::Request.new(
      :username   => ENV["paypal_username"],
      :password   => ENV["paypal_password"],
      :signature  => ENV["paypal_signature"]
    )

    paypal_options = {
      no_shipping: true, # if you want to disable shipping information
      allow_note: false, # if you want to disable notes
      pay_on_paypal: true # if you don't plan on showing your own confirmation step
    }

    payment_request = Paypal::Payment::Request.new(
      :currency_code => :USD,   # if nil, PayPal use USD as default
      :description   => "Order #{@order.id}",
      :quantity      => 1,      # item quantity
      :amount        => @order.totalPrice.to_f,
      :request_id    => @order.id.to_i,
      :custom_fields => {
        CARTBORDERCOLOR: "C00000",
        LOGOIMG: "https://www.anony.ws/i/2016/05/19/pph.png"
      }
    )

    session[:cart_description] = "Order #{@order.id}"
    session[:cart_amount] = @order.totalPrice.to_f
    session[:cart_order_id] = @order.id.to_i

    response = request.setup(
      payment_request,
      root_url + payment_confirm_path,
      root_url + payment_cancel_path,
      paypal_options
    )

    redirect_to response.redirect_uri

  end

  def confirm
    request = Paypal::Express::Request.new(
      :username   => ENV["paypal_username"],
      :password   => ENV["paypal_password"],
      :signature  => ENV["paypal_signature"]
    )

    payment_request = Paypal::Payment::Request.new(
      :currency_code => :USD,   # if nil, PayPal use USD as default
      :description   => session[:cart_description],    # item description
      :quantity      => 1,      # item quantity
      :amount        => session[:cart_amount],   # item value
      :request_id    => session[:cart_order_id],
      :custom_fields => {
        CARTBORDERCOLOR: "C00000",
        LOGOIMG: "https://www.anony.ws/i/2016/05/19/pph.png"
      }
    )

    response = request.checkout!(
      params[:token],
      params[:PayerID],
      payment_request
    )

    @payment = Payment.create(
      status: response.ack ,
      code: response.payment_info[0].transaction_id ,
      user_id: (current_user.id if current_user),
      order_id: response.payment_info[0].request_id,
      amount: response.payment_info[0].amount.total
    )


    redirect_to order_path(@payment.order_id)
  end

  def cancel
    render json: params
  end

  private
  def activeRecordToPaypalItems
    final = []
    @items.each do |item|
      item.id
    end
  end
end
