class Admin::TracksController < ApplicationController

  before_action :track_params, only: [:create]

  def create
    track = Track.new({
                      url: params[:url],
                      title: pretty_name(params[:filename]),
                      amazonId: params[:unique_id],
                      disc_id: params[:id]
                    })
    if track.save
      disc_interpreters = Disc.find(params[:id]).interpreter_ids
      track.interpreter_ids = disc_interpreters
      render json:{status: :success}
    else
      render json:{errors: track.errors}
    end
  end

  def edit
    @track = Track.find params[:id]
  end

  def update
    @track = Track.find params[:id]
    if @track.update track_params_edit
      redirect_to edit_admin_track_path(@track.id), notice: 'Track was successfully updated.'
    else
      render :edit
    end
  end

  def updateSampleUrl
      track = Track.find(params[:id])
      if track.update(:sampleUrl => params[:update][:sampleUrl])
        render json:{status: :success}
      else
        render json:{errors: track.errors}
      end
  end



  def track_params_edit
    params.fetch(:track, {}).permit(:title,:price,:interpreter_ids => [])
  end

  def destroy
    track = Track.find(params[:id])
    redirect_disc_id = track.disc_id

    if Track.destroy(params[:id])
      session[:success] = "Track Deleted"
      redirect_to edit_admin_disc_path(redirect_disc_id)
    end

  end
  private

  def track_params
    params.permit(:url,:unique_id,:filename,:id,:sampleUrl)
  end

  def pretty_name track_name
    pretty_name = File.basename(track_name,File.extname(track_name))
    pretty_name.titleize
    return pretty_name
  end

end
