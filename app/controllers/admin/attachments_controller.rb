class Admin::AttachmentsController < ApplicationController
  before_action :attachment_params, only: [:create]

  def create
    attachment = Attachment.new({
                      url: params[:url],
                      amazonId: params[:unique_id],
                      attachmentable_id: params[:attachmentable_id],
                      attachmentable_type: params[:attachmentable_type]
                    })
    if attachment.save
      render json:{status: :success}
    else
      render json:{errors: attachment.errors}
    end
  end

  def destroy
    attachment = Attachment.find(params[:id])
    redirect_type_id = attachment.attachmentable_id
    redirect_type = attachment.attachmentable_type

    if attachment.destroy
      session[:success] = "Cover Deleted"
      if redirect_type == "Disc"
        redirect_to edit_admin_disc_path(redirect_type_id)
      elsif redirect_type == "Author"
        redirect_to edit_admin_author_path(redirect_type_id)
      elsif redirect_type == "Genre"
        redirect_to edit_admin_genre_path(redirect_type_id)
      elsif redirect_type == "Admin::Featured"
        redirect_to edit_admin_featured_path(redirect_type_id)
      end

    end

  end

  def attachment_params
    params.permit(:url,:unique_id,:filename,:attachmentable_id,:attachmentable_type)
  end

end
