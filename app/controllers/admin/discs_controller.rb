
class Admin::DiscsController < ApplicationController
  before_action :require_login
  before_action :admin?
  before_action :set_admin_disc, only: [:show, :edit, :update, :destroy]

  # GET /admin/discs
  # GET /admin/discs.json
  def index
    @admin_discs = Disc.all

  end

  # GET /admin/discs/1
  # GET /admin/discs/1.json
  def show
  end

  # GET /admin/discs/new
  def new
    @admin_disc = Disc.new
  end

  # GET /admin/discs/1/edit
  def edit
    @genres = Genre.all
    @authors = Author.all
    @tracks = Track.where(:disc_id => params[:id]).order(:title)
    @covers = Attachment.where(:attachmentable_id => params[:id]).where(:attachmentable_type => 'Disc')
  end

  # POST /admin/discs
  # POST /admin/discs.json
  def create
    @admin_disc = Disc.new(admin_disc_params)
    respond_to do |format|
      if @admin_disc.save
        format.html { redirect_to admin_disc_url(@admin_disc) , notice: 'Disc was successfully created.' }
        format.json { render :show, status: :created, location: @admin_disc }
      else
        format.html { render :new }
        format.json { render json: @admin_disc.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/discs/1
  # PATCH/PUT /admin/discs/1.json
  def update
    respond_to do |format|
      if @admin_disc.update(admin_disc_params)
        format.html { redirect_to admin_disc_url(@admin_disc), notice: 'Disc was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_disc }
      else
        format.html { render :edit }
        format.json { render json: @admin_disc.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/discs/1
  # DELETE /admin/discs/1.json
  def destroy
    @admin_disc.destroy
    respond_to do |format|
      format.html { redirect_to admin_discs_url, notice: 'Disc was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_disc
      @admin_disc = Disc.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_disc_params
      params.fetch(:disc, {}).permit(:title_es,:title_en,:price,:description_es,:description_en,:genre_id ,:discUrl,:author_ids => [],:interpreter_ids => [])
      #params.require(:disc).permit(:title,:price,:description)
    end
end
