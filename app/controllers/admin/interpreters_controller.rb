class Admin::InterpretersController < ApplicationController
  before_action :set_admin_interpreter, only: [:show, :edit, :update, :destroy]

  # GET /admin/interpreters
  # GET /admin/interpreters.json
  def index
    @admin_interpreters = Interpreter.all
  end

  # GET /admin/interpreters/1
  # GET /admin/interpreters/1.json
  def show
  end

  # GET /admin/interpreters/new
  def new
    @admin_interpreter = Interpreter.new
  end

  # GET /admin/interpreters/1/edit
  def edit
  end

  # POST /admin/interpreters
  # POST /admin/interpreters.json
  def create
    @admin_interpreter = Interpreter.new(admin_interpreter_params)

    respond_to do |format|
      if @admin_interpreter.save
        format.html { redirect_to admin_interpreter_path(@admin_interpreter), notice: 'Interpreter was successfully created.' }
        format.json { render :show, status: :created, location: @admin_interpreter }
      else
        format.html { render :new }
        format.json { render json: @admin_interpreter.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/interpreters/1
  # PATCH/PUT /admin/interpreters/1.json
  def update
    respond_to do |format|
      if @admin_interpreter.update(admin_interpreter_params)
        format.html { redirect_to edit_admin_interpreter_path(@admin_interpreter.id), notice: 'Interpreter was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_interpreter }
      else
        format.html { render :edit }
        format.json { render json: @admin_interpreter.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/interpreters/1
  # DELETE /admin/interpreters/1.json
  def destroy
    @admin_interpreter.destroy
    respond_to do |format|
      format.html { redirect_to admin_interpreters_url, notice: 'Interpreter was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_interpreter
      @admin_interpreter = Interpreter.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_interpreter_params
      params.require(:interpreter).permit(:name_en,:name_es)
    end
end
