class StaticsController < ApplicationController
  def homepage
    @discs = Disc.includes(:genre,:attachments).last(4)
    @categories = Genre.where('collection = false').includes(:attachment,discs: :genre).last(3)
    @featureds = Admin::Featured.all
    @collections = Genre.where('collection = true').includes(:attachment).last(3)
  end

  def acerca_de

  end

end
