class LocalsController < ApplicationController
  def set
    locale = params[:id]
    raise 'unsupported locale' unless ['es', 'en'].include?(locale)
    session[:locale] = locale
    redirect_to :back
  end
end
