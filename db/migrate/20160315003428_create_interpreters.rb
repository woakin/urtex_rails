class CreateInterpreters < ActiveRecord::Migration
  def change
    create_table :interpreters do |t|
      t.string :name, null: false, uniqueness: true

      t.timestamps null: false
    end
  end
end
