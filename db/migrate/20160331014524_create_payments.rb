class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.belongs_to :order
      t.references :user, index: true
      t.string :amount, null: false
      t.string :status, null: false
      t.string :code, null: false
      t.timestamps null: false
    end

    add_foreign_key :payments, :users

  end
end
