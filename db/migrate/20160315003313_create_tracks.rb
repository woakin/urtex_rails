class CreateTracks < ActiveRecord::Migration
  def change
    create_table :tracks do |t|
      t.string :url, null: false
      t.string :title, null: false
      t.string :amazonId, null: false, uniqueness: true
      t.string :flocUrl
      t.belongs_to :disc, null: false
      t.timestamps null: false
    end
  end
end
