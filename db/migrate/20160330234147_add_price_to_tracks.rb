class AddPriceToTracks < ActiveRecord::Migration
  def change
    add_column :tracks, :price, :decimal, default: 12.00
  end
end
