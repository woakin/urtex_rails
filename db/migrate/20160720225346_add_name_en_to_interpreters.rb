class AddNameEnToInterpreters < ActiveRecord::Migration
  def change
    add_column :interpreters, :name_en, :string
  end
end
