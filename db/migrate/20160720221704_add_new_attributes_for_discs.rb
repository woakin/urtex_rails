class AddNewAttributesForDiscs < ActiveRecord::Migration
  def change
    add_column :discs, :title_en, :string
    add_column :discs, :description_en, :string
  end
end
