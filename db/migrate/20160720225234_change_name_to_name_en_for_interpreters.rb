class ChangeNameToNameEnForInterpreters < ActiveRecord::Migration
  def change
    rename_column :interpreters, :name, :name_es
  end
end
